Altium Designer Pick and Place Locations
C:\Users\Roma\AppData\Local\TempReleases\Snapshot\2\Assembly MOD_0\Pick Place\P&P_LORA_LC-2_INTERFACE_V3.0_MOD_0.txt

========================================================================================================================
File Design Information:

Date:       02.03.21
Time:       12:43
Revision:   502
Variant:    MOD_0
Units used: mm

Designator All                               Layer    Footprint            Center-X(mm) Center-Y(mm) Rotation Description                                                         
VT4        2N7002LT1G                        TopLayer SOT23                39.1357      62.6321      180      "Small Signal MOSFET"                                               
C14        "CERCAP 0402 0.33UF 10V X5R 10%"  TopLayer CAPCHIP0402          51.6522      41.7335      270      "Ceramic Capacitor"                                                 
U4         "PM IC V9240"                     TopLayer SOIC150-8_M          49.5909      50.2573      90       "ULTRALOW POWER, UART, SINGLE-PHASE, POWER MEASUREMENT IC"          
C23        "CAP CER 2.2UF 25V X5R 1206"      TopLayer CAPCHIP1206          39.0379      47.8037      270      "Ceramic Capacitor"                                                 
C22        "CERCAP 0402 0.1UF 50V X7R 10%"   TopLayer CAPCHIP0402          41.6080      42.4069      180      "Ceramic Capacitor"                                                 
VT3        BC847BS                           TopLayer SOT-363              22.1566      14.2419      360      "45 V, 100 mA NPN/NPN general-purpose transistor"                   
VT2        "PNP TRANSISTOR PZT2907A"         TopLayer SOT223               28.1990      12.3058      360      "PNP Silicon Epitaxial Transistor"                                  
VD9        MM3Z12VT1G                        TopLayer SOD-323              22.4659      19.1028      360      "Voltage regulator diodes"                                          
VD8        1N4148WS                          TopLayer SOD-323              27.9378      17.1627      360      "SURFACE MOUNT FAST SWITCHING DIODE"                                
VD6        1N4148WS                          TopLayer SOD-323              27.9057      19.1028      360      "SURFACE MOUNT FAST SWITCHING DIODE"                                
U3         LK112M33TR                        TopLayer SOT-23-5             42.5714      45.9536      360      "Low noise low drop voltage regulator with shutdown function"       
R28        "RES 0402 0.063W 10K 1%"          TopLayer RESCHIP0402          21.6953      17.3103      270      Resistor                                                            
R27        "RES 0402 0.063W 10K 1%"          TopLayer RESCHIP0402          21.6953      11.7543      270      Resistor                                                            
C21        "CERCAP 0402 0.1UF 50V X7R 10%"   TopLayer CAPCHIP0402          20.0575      13.7494      180      "Ceramic Capacitor"                                                 
C19        "CERCAP 0402 0.1UF 50V X7R 10%"   TopLayer CAPCHIP0402          43.5231      42.9340      180      "Ceramic Capacitor"                                                 
VT1        BC847BS                           TopLayer SOT-363              62.0700      25.7500      0        "45 V, 100 mA NPN/NPN general-purpose transistor"                   
VD2        MM3Z6V2T1G                        TopLayer SOD-323              62.0700      28.2500      360      "Voltage regulator diodes"                                          
VD1        1N4148WS                          TopLayer SOD-323              57.8700      32.5500      90       "SURFACE MOUNT FAST SWITCHING DIODE"                                
R26        "RES 0402 0.063W 10K 1%"          TopLayer RESCHIP0402          57.7700      13.3500      270      Resistor                                                            
DA6        TCMT1106                          TopLayer Mini_flat_half_pitch 59.5700      19.2500      180      "Optocoupler, Phototransistor Output"                               
C20        "CERCAP 0402 0.1UF 50V X7R 10%"   TopLayer CAPCHIP0402          21.6953      16.1955      270      "Ceramic Capacitor"                                                 
C18        "CERCAP 0402 0.1UF 50V X7R 10%"   TopLayer CAPCHIP0402          65.3700      25.3500      180      "Ceramic Capacitor"                                                 
FU3        375mA                             TopLayer FUSE_451_453_Series  34.6700      4.6500       180      "Resettable Fuses"                                                  
VD5        SMBJ26A-E3/52                     TopLayer DO-214AA_MELF_SMD    16.2124      35.0138      180      Transil�                                                            
R3         "RES 0402 0.063W 3K 1%"           TopLayer RESCHIP0402          62.5700      23.0700      360      Resistor                                                            
R2         "RES 0402 0.063W 3K 1%"           TopLayer RESCHIP0402          59.6700      25.3500      360      Resistor                                                            
R1         "RES 0402 0.063W 10K 1%"          TopLayer RESCHIP0402          58.4700      27.4500      360      Resistor                                                            
R14        "RES 1206 910K 1%"                TopLayer RESCHIP1206          60.6532      48.6866      270      Resistor                                                            
X2         "BH-10 (DS1013-10S)"              TopLayer BH-10(IDC-10MS)      17.1700      49.9400      360      "Connector, 10-Pin"                                                 
VD3        PMEG6030EP                        TopLayer SOD128               10.9359      20.7920      90       "3 A low VF MEGA Schottky barrier rectifier"                        
U2         P6AU-1205ELF                      TopLayer P6AU                 31.0770      47.3300      90       "DC-DC ���������������"                                             
R23        "RES 0402 0.063W 200R 1%"         TopLayer RESCHIP0402          64.3700      25.3500      360      Resistor                                                            
L1         "FIXED IND 10UH 650MA 240 MOHM"   TopLayer INDCHIP1812          25.2900      43.7700      180      "10uH power inductor, Isat = 560mA , Itemp = 710mA, Rdc = 0.360ohm" 
FU1        "FUSE 6A"                         TopLayer FUSE_3.6X10_D0.6_V   31.5278      35.1622      315      "Resettable Fuses"                                                  
C13        "CAP CER 2.2UF 25V X5R 1206"      TopLayer CAPCHIP1206          23.5100      49.4600      270      "Ceramic Capacitor"                                                 
C12        "CERCAP 0805 2.2UF 25V X7R 10%"   TopLayer CAPCHIP0805          42.5155      50.2508      360      "Ceramic Capacitor"                                                 
C11        "CERCAP 0402 0.1UF 50V X7R 10%"   TopLayer CAPCHIP0402          59.6700      27.4500      180      "Ceramic Capacitor"                                                 
C10        "CERCAP 0402 0.01UF 50V X7R 5%"   TopLayer CAPCHIP0402          42.9888      48.6724      90       "Ceramic Capacitor"                                                 
C9         "CERCAP 0603 10UF 25V X5R 20%"    TopLayer CAPCHIP0603          40.1301      42.7848      90       "Ceramic Capacitor"                                                 
R5         "RES 0402 0.063W 1K 1%"           TopLayer RESCHIP0402          36.6700      59.1630      90       Resistor                                                            
R16        "RES 0402 0.063W 1K 1%"           TopLayer RESCHIP0402          49.5909      44.1293      180      Resistor                                                            
R15        "RES 1206 910K 1%"                TopLayer RESCHIP1206          57.6632      51.8766      90       Resistor                                                            
R13        "RES 1206 910K 1%"                TopLayer RESCHIP1206          57.7162      45.5466      90       Resistor                                                            
R11        "RES 0402 0.063W 430R 5%"         TopLayer RESCHIP0402          25.6500      58.4000      360      Resistor                                                            
R10        "RES 1206 0.063W 100R 1%"         TopLayer RESCHIP0402          41.4383      38.0036      270      Resistor                                                            
R9         "RES 0402 0.063W 1K 1%"           TopLayer RESCHIP0402          25.3301      63.2894      90       Resistor                                                            
R7         "RES 0402 0.063W 200R 1%"         TopLayer RESCHIP0402          42.5597      60.1551      270      Resistor                                                            
R6         "RES 0402 0.063W 430R 5%"         TopLayer RESCHIP0402          36.6700      60.4630      270      Resistor                                                            
R8         "SMD 2512 R015"                   TopLayer RESCHIP2512          37.2900      36.2300      135      Resistor                                                            
R4         "RES 1206 0.063W 100R 1%"         TopLayer RESCHIP0402          41.4383      38.9770      270      Resistor                                                            
DA2        TCMT1106                          TopLayer Mini_flat_half_pitch 30.9870      58.5280      270      "Optocoupler, Phototransistor Output"                               
DA1        TCMT1106                          TopLayer Mini_flat_half_pitch 30.9870      62.6500      90       "Optocoupler, Phototransistor Output"                               
C8         "CERCAP 0603 10UF 25V X5R 20%"    TopLayer CAPCHIP0603          47.5701      56.5684      270      "Ceramic Capacitor"                                                 
C7         "CERCAP 0402 0.1UF 50V X7R 10%"   TopLayer CAPCHIP0402          48.9559      56.0684      360      "Ceramic Capacitor"                                                 
C6         "CERCAP 0603 1UF 25V X7R 10%"     TopLayer CAPCHIP0603          51.4959      56.5684      270      "Ceramic Capacitor"                                                 
C5         "CERCAP 0402 0.1UF 50V X7R 10%"   TopLayer CAPCHIP0402          47.5701      44.1293      180      "Ceramic Capacitor"                                                 
C4         "CERCAP 0603 1UF 25V X7R 10%"     TopLayer CAPCHIP0603          46.0487      44.6293      90       "Ceramic Capacitor"                                                 
C3         "CERCAP 0402 0.033UF 16V X7R 10%" TopLayer CAPCHIP0402          48.5429      44.1293      180      "Ceramic Capacitor"                                                 
C2         "CERCAP 0402 0.33UF 10V X5R 10%"  TopLayer CAPCHIP0402          52.6898      44.1092      360      "Ceramic Capacitor"                                                 
C1         "CERCAP 0402 0.33UF 10V X5R 10%"  TopLayer CAPCHIP0402          50.6959      44.1293      180      "Ceramic Capacitor"                                                 
